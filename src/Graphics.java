import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.text.Font;
import model.Model;
import model.Pillar;

public class Graphics {

    // Eigenschaften
    private Model model;
    private GraphicsContext gc;
    private Image pillar;
    private ImagePattern pillarPattern;
    private Image bird;
    private ImagePattern birdPattern;
    private Image background;
    private ImagePattern backgroundPattern;


    // Konstruktoren
    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;
        pillar = new Image("model/pillar.png");
        pillarPattern = new ImagePattern(pillar);
        bird = new Image("model/flying.gif");
        birdPattern = new ImagePattern(bird);
        background = new Image("model/background.jpg");
        backgroundPattern = new ImagePattern(background);
    }

    // Methoden
    public void draw() {

        if (!model.getPlayer().isCollided()) {
            clearScreen();
            drawBackground();
            drawPlayer();
            drawPillars();
            drawGround();
            drawScore();
        }
        else {
            drawGameOver();
        }

        if (!model.isStart()){
            drawStart();

        }
    }

    private void drawStart(){
        gc.setFill(Color.BLACK);
        gc.setFont(new Font(48));
        gc.fillText("Press \"Space\" to start", model.WIDTH/2-290,model.HEIGHT/2);

    }

    private void drawBackground(){
        gc.setFill(backgroundPattern);
//        gc.fillRect(
//                0,
//                0,
//                model.WIDTH,
//                model.HEIGHT
//        );

        gc.fillRect(
                - (model.getBackground().getTransition()),
                0,
                model.WIDTH,
                model.HEIGHT
        );
        gc.fillRect(
                - (model.getBackground().getTransition())+model.WIDTH,
                0,
                model.WIDTH,
                model.HEIGHT
        );
    }

    private void drawScore() {
        // Draw Score
        gc.setFill(Color.BLACK);
        gc.setFont(new Font(48));
        gc.fillText("Score: " + model.getScore(), model.WIDTH - 250, 50);
    }

    private void drawGameOver(){
        gc.setFill(Color.RED);
        gc.setFont(new Font(96));
        gc.fillText("GAMEOVER", model.WIDTH/2-300,model.HEIGHT/2-50);


        gc.setFill(Color.BLACK);
        gc.setFont(new Font(48));
        gc.fillText("Press \"Space\" to restart", model.WIDTH/2-290,model.HEIGHT/2);
    }

    private void drawGround() {
        // Draw Ground
        gc.setFill(Color.SANDYBROWN);
        gc.fillRect(
                model.getGround().getX(),
                model.getGround().getY(),
                model.getGround().getW(),
                model.getGround().getH()
        );

        gc.setFill(Color.LIGHTGREEN);
        gc.fillRect(
                model.getGround().getX(),
                model.getGround().getY(),
                model.getGround().getW(),
                10
        );

        for (int i = 0; i < 20; i++) {
            gc.setFill(Color.GREEN);
            gc.fillRect(
                    model.getGround().getX() + ((i + 1) * 200 - (10 * model.getGround().getTransition())),
                    model.getGround().getY(),
                    100,
                    10
            );


        }
    }

    private void drawPillars() {
        // Draw Pillar
        gc.setFill(pillarPattern);
        for (Pillar pillar : model.getPillars()) {
            gc.fillRect(
                    pillar.getX(),
                    pillar.getY(),
                    pillar.getW(),
                    pillar.getH()
            );

            if (pillar.getY() == 0) {
                gc.fillRect(
                        pillar.getX() - 10,
                        pillar.getY() + pillar.getH() - 40,
                        pillar.getW() + 20,
                        40
                );
            } else {

                gc.fillRect(
                        pillar.getX() - 10,
                        pillar.getY(),
                        pillar.getW() + 20,
                        40
                );
            }
        }
    }

    private void drawPlayer() {
        // Draw Player
        gc.setFill(birdPattern);
        gc.fillOval(
                model.getPlayer().getX() - model.getPlayer().getW() / 2,
                model.getPlayer().getY() - model.getPlayer().getH() / 2,
                model.getPlayer().getW() * 2,
                model.getPlayer().getH() * 2
        );
    }

    private void clearScreen() {
        // Clear Screen
        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);
    }


}
