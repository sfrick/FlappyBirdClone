import javafx.scene.input.KeyCode;
import model.Model;

public class InputHandler {

    // Eigenschaften
    private Model model;

    // Konstruktoren
    public InputHandler (Model model) {
        this.model = model;
    }

    // Methoden
    public void onKeyPressed(KeyCode key) {
        if (key == KeyCode.SPACE) {
            model.getPlayer().jump();

            if (model.getPlayer().isCollided()){
                model.reset();
            }
            if (!model.isStart()){
                model.setStart(true);
            }
        }
    }


}
