package model;

import java.util.Random;

public class PillarFactory {
    private static final Random RND = new Random(123);

    public PillarFactory(Model model) {
    }

    public static void addPillar(Model model) {

        int height = Model.HEIGHT - (int) (RND.nextInt(Model.HEIGHT / 2));
        model.getPillars().add(new Pillar(Model.WIDTH, height, 75, height + Model.HEIGHT));
        model.getPillars().add(new Pillar(Model.WIDTH, 0, 75, height - 200));

    }
}