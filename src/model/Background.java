package model;

public class Background implements GameObject {

    private int x;
    private int y;
    private int h;
    private int w;
    private int transition;

    public Background(int x, int y, int h, int w){
        this.x = x;
        this.y = y;
        this.h = h;
        this.w = w;
        this.transition = 0;
    }

    public void update(int width){
        if (getTransition() == width){
            resetTransition();
        }
        this.transition++;
    }


    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public int getH() {
        return h;
    }

    @Override
    public int getW() {
        return w;
    }

    public int getTransition() {
        return transition;
    }

    public void resetTransition(){
        this.transition = 0;
    }
}
