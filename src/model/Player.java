package model;

public class Player implements GameObject {

    // Eigenschaften
    private int x;
    private int y;
    private int h;
    private int w;
    private float speed;
    private boolean collided;

    // Konstruktoren
    public Player(int x, int y) {
        this.x = x;
        this.y = y;
        this.h = 50;
        this.w = 50;
        this.speed = 0;
        this.collided = false;
    }

    // Methoden
    public void update() {
        acceleration();
        move(0, (int) this.speed);
    }

    public void acceleration() {
        this.speed += 1.2;
    }

    public void move(int dx, int dy) {
        this.x += dx;
        this.y += dy;
    }

    public void jump() {
        this.speed = -15;
    }



    // Getter + Setter
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public boolean isCollided() {
        return collided;
    }

    public void setCollided(boolean collided) {
        this.collided = collided;
    }
}
