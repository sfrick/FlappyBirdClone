package model;

public class Pillar implements GameObject {

    private int x;
    private int y;
    private int h;
    private int w;

    public Pillar(int x, int y, int w, int h) {
        this.h = h;
        this.x = x;
        this.y = y;
        this.w = w;
    }


    public void move(int dx, int dy) {
        this.x += dx;
        this.y += dy;
    }

    public void update(long elapsedTime) {
        move(-10, 0);
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }
}
