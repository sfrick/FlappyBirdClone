package model;

public class Ground implements GameObject {

    private int x;
    private int y;
    private int h;
    private int w;
    private long transition;

    public Ground(int x, int y, int h, int w) {
        this.x = x;
        this.y = y;
        this.h = h;
        this.w = w;
        this.transition = 0;
    }

    public void update(){
        if (this.transition != 20)
            this.transition++;
        else
            this.transition = 0;
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public int getH() {
        return h;
    }

    @Override
    public int getW() {
        return w;
    }

    public long getTransition() {
        return transition;
    }
}
