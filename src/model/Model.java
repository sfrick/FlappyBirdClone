package model;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Model {

    // FINALS
    public static final int WIDTH = 1280;
    public static final int HEIGHT = 800;

    private final Random RND = new Random();
    private final PillarFactory pillarFactory = new PillarFactory(this);

    // Eigenschaften
    private Player player;
    private List<Pillar> pillars;
    private Ground ground;
    private Background background;
    private long time;
    private long totalTime;
    private int level;

    private boolean start;

    // Konstruktoren

    public Model() {
        this.player = new Player((int) (HEIGHT / 3), 120);
        this.pillars = new LinkedList<Pillar>();
        this.time = 0;
        this.ground = new Ground(0, HEIGHT - 50, 50, WIDTH);
        this.start = false;
        this.level = 1;
        this.background = new Background(0, 0, this.HEIGHT, this.WIDTH);
    }
    // Methoden

    public void reset(){
        this.player = new Player((int) (HEIGHT / 3), 120);
        this.pillars = new LinkedList<Pillar>();
        this.time = 0;
        this.ground = new Ground(0, HEIGHT - 50, 50, WIDTH);
        this.totalTime = 0;
        this.level = 1;
    }
    private void checkCollision() {
        // Kollisionsabfragen
        for (Pillar p : pillars) {
            if (CollisionDetection.detectCollision(player, p)) {
                player.setCollided(true);
            }
        }
        if (CollisionDetection.detectCollision(player, ground)) {
            player.setCollided(true);
        }
    }

    private void removePillars() {
        // Löschen der durchgelaufenen Säulen
        if (pillars.size() != 0 && pillars.get(0).getX() < -pillars.get(0).getW()) {
            pillars.remove(0);
            pillars.remove(0);
        }
    }

    private void updatePillars(long elapsedTime) {
        for (Pillar pillar : pillars) {
            pillar.update(elapsedTime);
        }
    }

    private void createPillars() {
        if (time / (1000/((double)level/4)) > 1) {
            pillarFactory.addPillar(this);
            time = 0;
        }
    }

    private void timeHandler(long elapsedTime) {
        totalTime += elapsedTime;
        time += elapsedTime;
        if (totalTime%10000 == 0)
            level++;
    }


    public void update(long elapsedTime) {

        checkCollision();
        removePillars();

        background.update(this.WIDTH);
        ground.update();
        player.update();

        timeHandler(elapsedTime);

        createPillars();
        updatePillars(elapsedTime);
    }



    // Setter + Getter
    public Player getPlayer() {
        return player;
    }

    public List<Pillar> getPillars() {
        return pillars;
    }

    public Ground getGround() {
        return ground;
    }

    public String getScore() {
        return String.valueOf(totalTime / 1000);
    }

    public Background getBackground() {
        return background;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public boolean isStart() {
        return start;
    }

    public void setStart(boolean start) {
        this.start = start;
    }
}
