package model;

public interface GameObject {

    int getX();

    int getY();

    int getH();

    int getW();
}
