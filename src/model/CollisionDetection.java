package model;


public class CollisionDetection {

    public static boolean detectCollision(GameObject o1, GameObject o2){
        return o1.getX() < o2.getX() + (o2.getW()) &&
                o2.getX() < o1.getX() + (o1.getW()) &&
                o1.getY() < o2.getY() + (o2.getH()) &&
                o2.getY() < o1.getY() + (o1.getH());
    }

}
